package co.edu.unimagdalena.quizlistviewautos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class AutoAdapter extends ArrayAdapter<Auto> {

    private final Context context;
    private final ArrayList<Auto> autos;
    private LayoutInflater inflater;

    public AutoAdapter(@NonNull Context context, ArrayList<Auto> autos) {
        super(context,0, autos);
        this.context = context;
        this.autos = autos;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Nullable
    @Override
    public Auto getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(R.layout.row_auto,null);
        }

        Auto auto = getItem(position);
        TextView marca = convertView.findViewById(R.id.row_marca);
        TextView modelo = convertView.findViewById(R.id.row_modelo);
        TextView placa = convertView.findViewById(R.id.row_placa);
        marca.setText(auto.getMarca());
        modelo.setText(auto.getModelo());
        placa.setText(auto.getPlaca());
        return convertView;
    }
}
