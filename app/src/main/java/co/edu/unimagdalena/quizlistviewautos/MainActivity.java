package co.edu.unimagdalena.quizlistviewautos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Auto> autos = new ArrayList<Auto>();
    TextInputLayout marca, modelo, placa;
    Button add;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        marca = findViewById(R.id.marca);
        modelo = findViewById(R.id.modelo);
        placa = findViewById(R.id.placa);
        add = findViewById(R.id.btnagregar);
        list = findViewById(R.id.autolist);

        final AutoAdapter autoAdapter = new AutoAdapter(this, autos);

        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Auto auto = new Auto(
                  marca.getEditText().getText().toString(),
                  modelo.getEditText().getText().toString(),
                  placa.getEditText().getText().toString()
                );

                autos.add(auto);

                Toast.makeText(getApplicationContext(),"Auto agregado", Toast.LENGTH_LONG).show();

                list.setAdapter(autoAdapter);
            }
        });
    }
}